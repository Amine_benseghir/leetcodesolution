 import java.util.*;

public class Easy {
    //705
    class MyHashSet {

        boolean []arr;
        public MyHashSet() {
            arr = new boolean[10000001];
        }

        public void add(int key) {
            arr[key]=true;
        }

        public void remove(int key) {
            arr[key]=false;
        }

        public boolean contains(int key) {
            return arr[key];
        }
    }

    public static void main(String[] args) {

    }






    //1370
    public String sortString(String s) {

        StringBuilder sb = new StringBuilder();
        int count = s.length();
        int[] frequency = new int[26];
        for (int i =0;i<s.length();i++){
            frequency[s.charAt(i)-'a']++;
        }
        while (count>0){
            for (int i = 0 ;i<frequency.length;i++){
                if (frequency[i]!=0){
                    sb.append((char) (i+97));//97 is a in assci code
                      frequency[i]--;
                      count--;
                    }
            }
            for (int i = frequency.length-1;i>=0;i--){
                if (frequency[i]!=0){
                    sb.append((char)(i+97));//97 is a in assci code
                     frequency[i]--;
                    count--;
                }
            }
        }
        return sb.toString();
    }

    //2068
    public boolean checkAlmostEquivalent(String word1, String word2) {
        int[] map = new int[26];
        for (int i = 0; i < word1.length(); i++) {
            map[word1.charAt(i) - 'a']++;
            map[word2.charAt(i) - 'a']--;
        }
        for (int i = 0; i < map.length; i++) {
            if (Math.abs(map[i]) > 3) return false;
        }
        return true;
    }


    //1897
    public boolean makeEqual(String[] words) {

        int []counter=new int[26];
        for (String s:words){
            for (char ch:s.toCharArray()){
                counter[ch-'a']++;
            }
        }
        for (int i = 0;i<26;i++){
            if (counter[i]%words.length!=0){
                return false;
            }
        }
        return true;
    }

    //1189
    public int maxNumberOfBalloons(String text) {

      int[]charCounts=new int[26];
      for (int i = 0;i<text.length();i++){
          charCounts[text.charAt(i)-'a']++;

      }
          int min = charCounts[1];//b
          min = Math.min(min,charCounts[0]);//a
          min = Math.min(min,charCounts[11]/2);//l divided by 2 because there's 2l
          min = Math.min(min,charCounts[14]/2);//o
          min = Math.min(min,charCounts[13]);//n
          return min;


    }



    //819
    public String mostCommonWord(String paragraph, String[] banned) {
        Set<String> set = new HashSet<>();
        for (String s:banned){
            if (!set.contains(s)){
                set.add(s);
            }
        }
        Map<String,Integer>map=new HashMap<>();
         String[]strs = paragraph.toLowerCase().split("\\W+");
        int max=0;
        String res = "";
        for (String str:strs){
            if (!set.contains(str)){
                map.put(str,map.getOrDefault(str,0)+1);
            }
        }
        for (String key : map.keySet()){
            if (map.get(key)>max){
                max=map.get(key);
                res=key;
            }
        }
        return res;
    }


    //2085
    public int countWords(String[] words1, String[] words2) {
      Map<String,Integer>map=new HashMap<>();
      for (int i = 0;i<words1.length;i++){
          if (!map.containsKey(words1[i])){
              map.put(words1[i],1);
          }else{
              map.put(words1[i],map.get(words1[i])+1);
          }
      }
      for (int i =0;i<words2.length;i++){
          if (map.containsKey(words2[i])&& map.get(words2[i])==1){
              map.put(words2[i],0);
          }else{
              map.put(words2[i],-1);
          }

      }
          int count=0;
          for(HashMap.Entry<String,Integer> entry : map.entrySet()) {
             if (entry.getValue()==0){
                 count++;
             }
      }
      return count;
    }




    //1941
    public boolean areOccurrencesEqual(String s) {
       int[]counts =new int[26];
       int max =0;
       for (char c:s.toCharArray()){
           counts[c-'a']++;
           max= Math.max(max,counts[c-'a']);
       }
       for (int count:counts){
           if (count!=0 && count!=max){
               return false;
           }
       }
       return true;
    }




    //1790
    public boolean areAlmostEqual(String s1, String s2) {

        int len1=s1.length();
        int len2 = s2.length();
        if (len1!=len2){
            return false;
        }
        int diffNum=0;
        int []idx=new int[2];
        for (int i =0;i<len1;i++){
            if (s1.charAt(i) != s2.charAt(i)) {
                if (diffNum >= 2) {
                    return false;
                }

                idx[diffNum++] = i;
            }
        }
        if (diffNum==0){
            return true;
        }
        if (diffNum==2){
            if (s1.charAt(idx[0])==s2.charAt(idx[1])
                    && s1.charAt(idx[1])==s2.charAt(idx[0])){
                return true;
            }
        }
        return false;
    }




        //724
    public int pivotIndex(int[] nums) {

      int totalSum=0;
      for(int i =0;i<nums.length;i++){
          totalSum+=nums[i];
      }
      int leftSum=0;
      for (int i =0;i<nums.length;i++){
          if (i!=0) leftSum+=nums[i-1];
          if (totalSum - leftSum - nums[i] == leftSum){
              return i;
          }
      }
      return -1;
    }

    //1480
    public int[] runningSum(int[] nums) {

        int []res=new int[nums.length];
        int sum=0;
        for (int i = 0;i<nums.length;i++){
           sum+=nums[i];
           res[i]=sum;
        }

        return res;
    }


    //1991
    public int findMiddleIndex(int[] nums) {

        if(nums.length==1){
            return 0;
        }

        int length = nums.length;
        int []leftPartSum= new int[length];
        int []rightPartSum = new int[length];
        leftPartSum[0]=nums[0];
        for (int i = 1;i<nums.length;i++){
            leftPartSum[i]=leftPartSum[i-1]+nums[i];

        }
        rightPartSum[length-1]=nums[length-1];
        for (int i=length-2;i>=0;i--){
            rightPartSum[i]=rightPartSum[i+1]+nums[i];
        }
        for (int i =0;i<length;i++){
            if (leftPartSum[i]==rightPartSum[i]){
                return i;
            }
        }
        return -1;
    }


    //1732
    public int largestAltitude(int[] gain) {
        int maxAlt=0;
        int alt=0;
        for (int climb:gain){
            alt+=climb;
            maxAlt=Math.max(maxAlt,alt);

        }
        return maxAlt;

    }



    //303
    int [] prefixSum =null;
    //added void to the method signature to avoid compilation error
    public void NumArray(int[] nums) {
      prefixSum=new int[nums.length+1];
      for (int i =0;i<nums.length;i++)
          prefixSum[i+1]=prefixSum[i]+nums[i];


    }

    public int sumRange(int left, int right) {
         return prefixSum[right+1]-prefixSum[left];
    }

    //2108
    public String firstPalindrome(String[] words) {
        for (int i = 0;i<words.length;i++){
            if (isPalandromic(words[i])){
                return words[i];
            }
        }
        return "";
    }
    public boolean isPalandromic(String str){
        int start=0;
        int end =str.length()-1;
        while (start<end){
            if (str.charAt(start)!=str.charAt(end)){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }


    //2000
    public String reversePrefix(String word, char ch) {
       int end = word.indexOf(ch);
       int len = word.length();
       StringBuilder prefix= new StringBuilder(word.substring(0,end+1));
       prefix.reverse();
       StringBuilder endPart= new StringBuilder(word.substring(end+1,len));
       return prefix.append(endPart).toString();
    }



    //541
    public String reverseStr(String s, int k) {
        char[] c =s.toCharArray();
        for (int i = 0;i<s.length();i+=2*k){
            int start=i;
            int end=Math.min(i+k-1,c.length-1);
            while (start<end){
                char temp = c[start];
                c[start++]=c[end];
                c[end--]=temp;
            }
        }
        return new String(c);
    }







    //350
    public int[] intersect(int[] nums1, int[] nums2) {

        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i=0;
        int j=0;
        List<Integer>list =new ArrayList<>();
        while (i<nums1.length && j< nums2.length){
            if (nums1[i]==nums2[j]){
                list.add(nums1[i]);
                i++;
                j++;
            }else if (nums1[i]<nums2[j]){
                i++;
            }else {
                j++;
            }

        }
        int []result = new int[list.size()];
        int k=0;
        for (int nums:list){
            result[k++]=nums;
        }
        return result;
    }

    //1748
    public int sumOfUnique(int[] nums) {
        int []freq = new int[101];
        int sum = 0;
        for (int i :nums){
            freq[i]++;
        }
        for (int i =0;i<freq.length;i++){
            if (freq[i]==1){
                sum+=i;
            }
        }
        return sum;
    }



    //2057
    public int smallestEqual(int []nums){
        int index=-1;
        for (int i = 0;i < nums.length; i++){
            if (i%10==nums[i]){
                index=i;
                break;
            }
        }
        return index;
    }




    //1534
    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int len = arr.length;
        int res = 0;
        for(int i=0; i<len; i++){
            for(int j=i+1; j<len; j++) {
                if(Math.abs(arr[i] - arr[j]) <= a) {  // skip when it failed the first case
                    for(int k=j+1; k<len; k++){
                        if(Math.abs(arr[j] - arr[k]) <= b) {
                            if(Math.abs(arr[i] - arr[k]) <= c) {
                                res++;
                            }
                        }
                    }
                }
            }
        }
        return res;
    }


    //2006
    public int countKDifference(int[] nums, int k) {
        int res=0;
        for(int i =0 ;i <nums.length;i++){
            for(int j=0;j<nums.length;j++){
                if(i<j && Math.abs(nums[i]-nums[j])==k){
                    res+=1;
                }
            }
        }
        return res;
    }




    //2144
    public int minimumCost(int[] cost) {

        int n=cost.length;
        if (n==1){
            return cost[0];
        }
        if (n==2){
            return cost[0]+cost[1];
        }
        Arrays.sort(cost);
        int s=0;
        for (int i=n-1;i>=0;i-=2){
            s+=cost[i];
            if (i>=1){
                s+=cost[i-1];
            }
            i--;
        }
        return s;


    }

      //1365
    public int[] smallerNumbersThanCurrent(int[] nums) {
       int []ans=new int[nums.length];
       for (int i=0;i<nums.length;i++){
           int count =0;
           int j=0;
           while (j<nums.length-1){
               if (nums[j]<nums[i]){
                   count++;
               }
               j++;
           }
           ans[i]=count;
       }
       return ans;
    }





    //2037
    public int minMovesToSeat(int[] seats, int[] students) {
        Arrays.sort(seats);
        Arrays.sort(students);
        int diff = 0;
        for(int i=0; i<seats.length; i++){
            diff +=  Math.abs(students[i]-seats[i]);
        }
        return diff;

    }





    //922
    public int[] sortArrayByParityII(int[] nums) {

        int i=0;
        int j=1;
        int n =nums.length;
        while (i<n && j<n){
           while (i<n && nums[i]%2==0){
               i+=2;
           }
           while (j<n && nums[j]%2==1){
               j+=2;
           }
           if (i<n && j<n){
               int temp = nums[i];
               nums[i]=nums[j];
               nums[j]=temp;
           }
        }
        return nums;

    }





    //1913
    public int maxProductDifference(int[] nums) {

        Arrays.sort(nums);
        int maxPairProduct=nums[nums.length-1]*nums[nums.length-2];
        int minPairProduct=nums[0]*nums[1];
        return maxPairProduct-minPairProduct;

    }


    //1051
    public int heightChecker(int[] heights) {
            int []sortedArray=heights.clone();
            int count=0;
            Arrays.sort(sortedArray);
            for (int i=0;i<heights.length;i++){
                if (heights[i]!=sortedArray[i]){
                    count++;
                }
            }
            return count;
    }

    //561
    public int arrayPairSum(int[] nums) {

        int sum=0;
        Arrays.sort(nums);
        for (int i =0;i<nums.length;i+=2){
            sum+=nums[i];
        }
        return sum;
    }
    //888
    public int[] fairCandySwap(int[] aliceSizes, int[] bobSizes) {

        int []result=new int [2];
        int totalAlice=0;
        int totalBob=0;
        Set<Integer> setB=new HashSet<>();
        for (int a:aliceSizes){
            totalAlice+=a;
        }
        for (int b :bobSizes){
            totalBob+=b;
            setB.add(b);
        }
        int delta=(totalBob-totalAlice)/2;
        for (int a :aliceSizes){
            if (setB.contains(a+delta)){
                result[0]=a;
                result[1]=a+delta;
                return result;
            }
        }
        return null;
    }



    //643

    public double findMaxAverage(int[] nums, int k) {
        int max = 0;
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += nums[i];
        }
        max = sum;
        for (int i = k; i < nums.length; i++) {
            sum = sum + nums[i] - nums[i - k];
            max = Math.max(max, sum);
        }
        return (double)(max) / k;
    }


    //942
    public int[] diStringMatch(String s) {

        int size =s.length();
        int low=0;
        int high=size;
        int[]output_arr=new int[size+1];
        for (int i=0;i<size;i++){
            if (s.charAt(i)=='I'){
                output_arr[i]=low++;
            }else {
                output_arr[i]=high--;
            }
        }
        output_arr[size]=low;
        return output_arr;

    }





    //414
    public int thirdMax(int[] nums) {
        Integer max =null;
        Integer secondMax=null;
        Integer thirdMax=null;
        for (Integer num:nums){
            if (num.equals(max) || num.equals(secondMax) || num.equals(thirdMax)){
                continue;
            }

            if (max==null || num>max){
                thirdMax=secondMax;
                secondMax=max;
                max=num;
            }else if (secondMax==null ||num>secondMax){
                thirdMax=secondMax;
                secondMax=num;
            }else if (thirdMax==null || num>thirdMax){
                thirdMax=num;
            }

        }
        if (thirdMax==null){
            return max;
        }
        return thirdMax;

    }


    //744
    public char nextGreatestLetter(char[] letters, char target) {
        int n = letters.length;
        if (letters[n-1]<=target || target<letters[0]){
            return letters[0];
        }
        int l=0;
        int r=n-1;
        while (l+1<r){
            int mid = l+(r-l)/2;
            if (letters[mid]<=target){
                l=mid;
            }else {
                r=mid;
            }
        }
        return letters[r];
    }





    //53
    public int maxSubArray(int[] nums) {

        int sum=nums[0];
        int maxSum =nums[0];
        for (int i =1;i<nums.length;i++){
            if (sum<0){
                sum=nums[i];
            }else{
                sum+=nums[i];
            }
            maxSum= Math.max(sum,maxSum);
        }

        return maxSum;
    }



    //35
    public int searchInsert(int[] nums, int target) {

        int start = 0;
        int end =nums.length-1;
        while (start<=end){
            int mid =start+(end-start)/2;
            if (nums[mid]==target){
                return mid;
            }else if (nums[mid]>target){
                end=mid-1;
            }else{
                start=mid+1;
            }
        }
        return start;
    }








    //575
    public int distributeCandies(int[] candyType) {
        Set<Integer> kinds = new HashSet<>();
        for (int candy : candyType){
            kinds.add(candy);
        }
        return Math.min(kinds.size(), candyType.length / 2);
    }

    //557
    public String reverseWords(String s) {
        int n = s.length();
        char chars []=s.toCharArray();
        for (int i =0;i<n;){
            int j=i;
            while (j<n && chars[j] !=' '){
                j++;
            }
            reverse(chars,i,j-1);
            i=j+1;
        }

        return new String(chars);
    }
    public void reverse(char chars[],int start, int end){
        int i=start;
        int j=end;

        while (i<j){
            char temp=chars[i];
            chars [i]=chars[j];
            chars[j]=temp;
            i++;
            j--;
        }
    }

    //434
    public int countSegments(String s) {
        int segments= 0;
        for (int i =0;i<s.length();i++){
            if ((i==0 ||s.charAt(i-1)==' ') && s.charAt(i)!=' '){
                segments++;
            }
        }
        return segments;
    }

    //58
    public int lengthOfLastWord(String s) {

        int count = 0;
        if(s.length()==0){
            return count;
        }
        int currentIndex=s.length()-1;
        while (currentIndex>=0){
            if(s.charAt(currentIndex)!=' '){
                break;
            }
            currentIndex--;
        }
        for (int i =currentIndex;i>=0;i--){
            if (s.charAt(i)==' '){
                break;
            }
            count++;
        }
        return count;

    }

    //704
    public int search(int[] nums, int target) {
        int left = 0;
        int right=nums.length-1;
        while (left<=right){
            int mid =left+(right-left)/2;
            if (nums[mid]==target){
                return mid;
            }else if (nums[mid]>target){
                right=mid-1;
            }else {
                left=mid+1;
            }
        }
        return -1;

    }


    //167
    public int[] twoSum2(int[] numbers, int target) {
        int pointer_a=0;
        int pointer_b=numbers.length-1;
        for (int i =0;i<=numbers.length;i++) {
            int sum=numbers[pointer_a]+numbers[pointer_b];
            if (sum>target){
                pointer_b-=1;
            }else if (sum<target){
                pointer_a +=1;
            }else {
                return new int[]{pointer_a+1,pointer_b+1};
            }
        }

        return new int[]{pointer_a+1,pointer_b+1};
    }


    //929
    public int numUniqueEmails(String[]emails){
        Set<String> set=new HashSet<>();
        for(String s:emails){
            StringBuffer temp=new StringBuffer();
            int i=0;
            while(s.charAt(i)!='+' && s.charAt(i)!='@'){
                if(s.charAt(i)=='.'){
                    i++;
                    continue;
                }
                temp.append(s.charAt(i));
                i++;
            }
            while(s.charAt(i)!='@')
                i++;
            temp.append(s.substring(i,s.length()-1));
            set.add(temp.toString());
        }
        return set.size();
    }

    //169
    public int majorityElement(int[] nums) {
        if (nums.length==1){
            return nums[0];
        }

        Map<Integer,Integer> map=new HashMap<>();
        for (int i :nums){
            if (map.containsKey(i)&&map.get(i)+1>nums.length/2){
                return i;
            }else {
                map.put(i,map.getOrDefault(i,0)+1);
            }
        }
        return -1;
    }



    //252
    public boolean canAttendMeetings(Interval[]intervals){
        int []starts=new int[intervals.length];
        int [] end = new int[intervals.length];
        for (int i = 0;i<intervals.length;i++){
            starts[i]=intervals[i].start;
            end[i] = intervals[i].end;
        }
        Arrays.sort(starts);
        Arrays.sort(end);

        for (int i =0;i<starts.length-1;i++){
            if (starts[i+1]<end[i]){
                return false;
            }
        }
        return true;

    }




    //448
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer>missingNumbers=new ArrayList<>();
        Set<Integer>numbers = new HashSet<>();
        for (int i :nums){
            numbers.add(i);
        }
        for (int i = 1;i<=nums.length;i++){
            if (!numbers.contains(i)){
                missingNumbers.add(i);
            }
        }
        return missingNumbers;
    }




    //896
    public boolean isMonotonic(int[] nums) {

        boolean increasing= true;
        boolean decreasing= true;
        for (int i =0;i<nums.length-1;i++){
            if (nums[i]>nums[i+1]){
                increasing=false;
            }

            if (nums[i]<nums[i+1]){
                decreasing=false;
            }

        }
        return increasing ||decreasing;
    }




    //389
    public char findTheDifference(String s, String t) {
        int []alphabet = new int[26];
        for (char c :s.toCharArray()){
            alphabet[c-'a']++;
        }
        for (char c :t.toCharArray()){
            alphabet[c-'a']--;
            if (alphabet[c-'a']<0){
                return c;
            }
        }
        return '!';
    }






    //905
    public int[] sortArrayByParity(int[] nums) {

        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                int temp = nums[index];
                nums[index++] = nums[i];
                nums[i] = temp;
            }
        }
        return nums;
    }



    //26
    public int removeDuplicates(int[]nums){
        int index = 1;
        for (int i=0;i<nums.length-1;i++){
            if (nums[i]!=nums[i+1]){
                nums[index++]=nums[i+1];
            }
        }
        return index;
    }







    //520
    public boolean detectCapital(String word){
        int count =0;
        for (int i = 0;i<word.length();i++){
            if (Character.isUpperCase(word.charAt(i))){
                count++;
            }
        }
        return count==word.length() ||count==0 || count ==1 && Character.isUpperCase(word.charAt(0));
    }









    //219
    public boolean containsNearbyDuplicate(int[]nums,int k){
        Map<Integer,Integer>map = new HashMap<>();
        for (int i = 0;i<nums.length;i++){
            int current=nums[i];
            if (map.containsKey(current) && i-map.get(current)<=k ){
                return true;
            }else {
                map.put(current,i);
            }
        }
        return false;
    }





    //204
    public int countPrimes (int n){
        boolean []primes = new boolean[n];
        for (int i = 0;i<primes.length;i++){
            primes[i]=true;

        }
        for (int i = 2;i*i< primes.length;i++){
            if (primes[i]){
                for (int j=i;j*i<primes.length;i++){
                    primes[i*j]=false;
                }
            }

        }
        int primeCount =0;
        for (int i=2;i<primes.length;i++){
            if (primes[i]){
                primeCount++;
            }
        }
        return primeCount;
    }





    //125
    public boolean palindrome(String s){
        int i = 0;
        int j=s.length()-1;
        while (i<j){
            while (i<j &&!Character.isLetterOrDigit(s.charAt(i))){
                i++;
            }
            while ((i<j &&!Character.isLetterOrDigit(s.charAt(j)))){
                j--;
            }
            if (i<j && Character.toLowerCase(s.charAt(i++))!=Character.toLowerCase(s.charAt(j--))){
                return false;
            }

        }
        return true;

    }





        //27
    public int removeElement(int []nums,int val){
        int index = 0;
        for (int i :nums){
            if (i!=val){
                nums[index++]=i;
            }
        }
        return index;
    }






    //268
    public int missingNumber(int []nums){
        int sum = 0;
        for (int i:nums){
            sum+=i;
        }
        int n = nums.length+1;
        return (n*(n-1)/2)-sum;
    }






    //349
    public int[]intersection(int []nums1,int []nums2){
        Set<Integer>set = new HashSet<>();
        for (int i :nums1){
            set.add(i);
        }
        Set<Integer>intersection = new HashSet<>();
        for (int i :nums2){
            if (set.contains(i)){
                intersection.add(i);
            }
        }
        int []result = new int[intersection.size()];
        int index = 0;
        for (int i:intersection){
            result[index++]=i;
        }
        return result;
    }






    //7
    public int reverseNum(int x){
        boolean negative = false;
        if (x<0){
            negative = true;
            x*=-1;
        }
        long reversed = 0;
        if (x>0){
            reversed= (reversed*10)+(x%10);
            x/=10;
        }
        if (reversed>Integer.MAX_VALUE){
            return 0;
        }
        return  negative?(int) (-1* reversed):(int)reversed;
    }




    //283
    public void moveZero(int []arr){
        if (arr==null ||arr.length<=1){
            return ;
        }
        int index=0;
        for (int i :arr){
            if (i!=0){
                arr[index++]=arr[i];
            }
        }
        for (int i = index;i<arr.length;i++){
            arr[i]=0;
        }
    }








    //231
    public boolean powerOfTwo(int n){
        long i=0;
        while (i<n){
            i*=n;
        }
        return i==n;

    }




    //136
    public int singleNumber(int []numbers){
        Set<Integer>set = new HashSet<>();
        for (int i : numbers){
            if (set.contains(i)){
                set.remove(i);
            }else {
                set.add(i);
            }
        }
        for (int i :set){
            return i;
        }
        return -1;
    }




    //412
    public List<String>fizzBuzz(int n){
        List<String>result = new ArrayList<>();
        for (int i = 0;i<n;i++){
            if (n % 3 == 0 && n % 5 == 0){
                result.add("fizzbuzz");
            }else if (n % 3 == 0){
                result.add("fizz");
            }else if (n % 5 ==0 ){
                result.add("buzz");

            }else {
                result.add(Integer.toString(i));
            }
        }
        return result;
    }






    //217
    public boolean findDuplicate(int []nums){
        Set<Integer>set = new HashSet<>();
        for (int i:nums){
            if (set.contains(i)){
                return true;
            }else {
                set.add(i);
            }
        }
        return false;
    }




    //121
    public int maxProfit2(int[] prices) {
        int maxProfit = 0;
        int minVal = Integer.MAX_VALUE;
        for (int i =0;i<prices.length;i++){
            if (prices[i]<minVal){
                minVal=prices[i];
            }else if (prices[i]-minVal>maxProfit){
                maxProfit=prices[i]-minVal;
            }
        }
        return maxProfit;
    }







    //387
    public int firstUniqueChar(String s) {

        int []alphabet = new int[26];
        for (int i =0;i<s.length();i++){
            alphabet[s.charAt(i)-'a']++;
        }
        for (int i = 0;i<s.length();i++){
            if (alphabet[i]==1){
                return i;
            }
        }
        return -1;
    }





    //242
    public boolean isAnagram(String s,String t){
        if (s.length()!= t.length()){
            return false;
        }
        int[]counts =new int[26];
        for (int i = 0;i< s.length();i++){
            counts[s.charAt(i)-'a']++;
            counts[t.charAt(i)-'a']--;
        }
        for (int i:counts){
            if (i!=0){
                return false;
            }
        }

        return true;
    }






    //344
    public String reverseString(String s){
        String revString = "";
        char []chars = s.toCharArray();
        for (int i = chars.length-1;i>0;i--){
            revString += chars[i];
        }
        return revString;

    }







    //383
    public boolean canConstruct(String ransomNote ,String magazine){
        Map<Character,Integer>counts= new HashMap<>();
        for (char   c : magazine.toCharArray()){
            counts.put(c,counts.getOrDefault(c,0)+1);
        }
        for (char c: ransomNote.toCharArray()){
            if (!counts.containsKey(c) || counts.get(c)<=0){
                return false;
            }
            counts.put(c,counts.get(c)-1);
        }
        return true;
    }







    //680
    public  boolean validPalindrome(String str){
        int start = 0;
        int end=str.length()-1;

        while (start<end){
            if (str.charAt(start)!=str.charAt(end)){
                return  isPalindrome(str,start+1,end)||isPalindrome(str,start,end-1);
            }
            start++;
            end--;
        }
        return true;
    }


    public boolean isPalindrome(String str,int start,int end){
        while (start<end){
            if (str.charAt(start++)!=str.charAt(end--)){
                return false;
            }
        }
        return true;
    }








    //14
    public String longestCommonPrefix(String[]str){
        String longestCommonPrefix = "";
        if (str ==null || str.length==0){
            return longestCommonPrefix;
        }
        int index = 0;
        for (char c:str[0].toCharArray()){
            for (int word =1;word<str[word].length();word++){
                if (index>=str[word].length() || c!=str[word].charAt(index)){
                    return longestCommonPrefix;

                }

            }
            longestCommonPrefix +=c;
            index++;

        }
        return  longestCommonPrefix;
    }






    //1108
    public String defangIpAddr(String s ){
        StringBuilder reuslt = new StringBuilder();
        for (int  i = 0;i<s.length();i++){
            char current = s.charAt(i);
            if (current=='.'){
                reuslt.append("[.]");
            }else {
                reuslt.append(current);
            }
        }
        return reuslt.toString();
    }






    //443
    public int compress(char[]chars){
        int index = 0;
        int i = 0;
        while (i<chars.length){
            int j=i;
            while (j< chars.length && chars[i]== chars[j]){
                j++;
            }
            chars[index++]=chars[i];
            if (j-i>1){
                String count = j-i + "";

                for (char c:count.toCharArray()){
                    chars[index++]=c;
                }
            }
            i=j;

        }
        return index;
    }





    //21
    public ListNode mergeTwoLists(ListNode l1,ListNode l2){
        ListNode tempNode = new ListNode(-1);
        ListNode currentNode = tempNode;
        while (l1!= null && l2!=null){
            if (l1.val < l2.val){
                currentNode.next=l1;
                l1=l1.next;
            }else {
                currentNode.next=l2;
                l2=l2.next;
            }
            tempNode = tempNode.next;

        }
        if (l1 != null){
            currentNode.next=l1;
        }else {
            currentNode.next=l2;
        }
        //return tempNode(0)
        return currentNode.next;
    }




    //941
     public boolean validMountainArray(int[]A){
        int i =0;
        while (i<A.length && i+1<A.length && A[i]<A[i+1]){
            i++;
        }
        if (i==0 || i+1>A.length){
            return false;
        }
        while (i<A.length && i+1>A.length ){
            if (A[i] <= A[i++ +1]){
                return false;
            }
        }
        return true;
    }




    //1119
    public String removeVowels(String s ){
        Set<Character>vowels = new HashSet<>();
        vowels.add('a');
        vowels.add('A');
        vowels.add('e');
        vowels.add('E');
        vowels.add('i');
        vowels.add('I');
        vowels.add('o');
        vowels.add('O');
        vowels.add('u');
        vowels.add('U');

        StringBuilder result = new StringBuilder();
        for (char c : s.toCharArray()){
            if (!vowels.contains(c)){
                result.append(c);

            }
        }
        return result.toString();

    }






    //257
    public List<String>binaryTreePaths(TreeNode root){
        List<String>paths = new ArrayList<>();
        if (root== null){
            return paths;
        }
        dfs(root,"",paths);
        return paths;
    }
    public void dfs(TreeNode root,String path,List<String>paths){
        path+= root.val;

        if (root.left == null && root.right == null){
            return;
        }
        if (root.left != null){
            dfs(root.left,path+" ->",paths);
        }
        if (root.right != null){
            dfs(root.right,path +"->",paths);
        }
    }







    //345
    public String reverseVowels(String word){
        Set<Character>vowels = new HashSet<>();
        vowels.add('a');
        vowels.add('A');
        vowels.add('e');
        vowels.add('E');
        vowels.add('i');
        vowels.add('I');
        vowels.add('o');
        vowels.add('O');
        vowels.add('u');
        vowels.add('U');
        char[]chars = word.toCharArray();
        int start = 0;
        int end = word.length()-1;
        while (start<end){
            while (start<end && !vowels.contains(chars[start])){
                start++;
            }
            while (start<end && !vowels.contains(chars[end])){
                end--;
            }
            char temp = chars[start];
            chars[start++]=chars[end];
            chars[end--]= temp;
        }
        return new String(chars);
    }







    //160
    public ListNode getIntersectionNode(ListNode headA,ListNode headB){
        Set<ListNode>visited = new HashSet<>();
        while (headA != null){
            visited.add(headA);
            headA=headA.next;

        }

        while (headB!=null){
            if (visited.contains(headB)){
                return headB;
            }
            headB=headB.next;

        }
        return null;
    }






    //1046
    public int lastWeightStones(int [] stones){
        PriorityQueue<Integer>maxHeap =new PriorityQueue<>();
        for (int stone:stones){
            maxHeap.add(-stone);
        }
        while (maxHeap.size()>1){
            int stoneOne = -maxHeap.remove();
            int stoneTwo = -maxHeap.remove();

            if (stoneOne!= stoneTwo){
                maxHeap.add(-(stoneOne-stoneTwo));
            }
        }
        return maxHeap.isEmpty()? 0 :-maxHeap.remove();
    }






    //122
    public int maxProfit(int [] prices){
        int maxProfit = 0;
        if (prices.length<=1){
            return maxProfit;
        }


        for (int i =1;i<prices.length;i++){
            if (prices[i]>prices[i-1]){
                maxProfit+=prices[i]-prices[i-1];
            }
        }
        return maxProfit;

    }


    //415
    public String addString(String num1,String num2){
        StringBuilder result = new StringBuilder();
        int i = num1.length()-1;
        int j = num2.length()-1;
        int carry = 0;
        while (i>=0||j>=0){
            int sum = carry;
            if (i>=0){
                sum+=num1.charAt(i--)-'0';
            }
            if (j>=0){
                sum+=num2.charAt(j--)-'0';
            }
            result.append(sum%10);
            carry=sum/10;

        }
        if (carry!=0){
            result.append(carry);

        }
        return result.reverse().toString();
    }




    //1221
    public int balencedStringSplit(String s){
        int balancedCount = 0;
        int count = 0;
        for (int i = 0;i<s.length();i++){
            char current = s.charAt(i);
            if (current=='R'){
                count++;
            }else if (current=='L'){
                count--;
            }
            if (count == 0){
                balancedCount++;
            }

        }
        return balancedCount;
    }




    //917
    public String reverseOnlyLetters(String word){
        char []ch = word.toCharArray();
        int i = 0;
        int j = word.length()-1;
        while (i<j){
            while (i<j && !Character.isLetter(word.charAt(i))){
                i++;
            }
            while (i<j && !Character.isLetter(word.charAt(j))){
                j--;
            }

            char c = ch[i];
            ch[i++] = ch[j];
            ch[j--] = c;
        }
        return new String(ch);
    }







    //953
    public boolean isAlienSorted(String[]words,String order) {

        int [] alphabet =new int[26];
        for (int i = 0;i<order.length();i++){
            alphabet[order.charAt(i)-'a']=i;
        }
        for (int i =0;i<words.length;i++){
            for (int j=i+1;j< words.length;j++){
                int min = Math.min(words[i].length(),words[j].length());
                for (int k =0;k<min;k++){
                    char iChar = words[i].charAt(k);
                    char jChar = words[j].charAt(k);
                    if (alphabet[iChar -'a']<alphabet[jChar-'a']){
                        break;
                    }else if (alphabet[jChar-'a']<alphabet[iChar-'a']){
                        return false;
                    }else if (k==min - 1 && words[i].length()>words[j].length()){
                        return false;
                    }


                }
            }
        }
        return true;
    }






    //572
    public boolean isSubTree(TreeNode s,TreeNode t){
        if (s==null){
            return false;
        }else if (isTheSame(s,t)){
            return true;
        }else{
            return (isSubTree(s.left,t))||(isSubTree(s.right,t));
        }
    }
    public boolean isTheSame(TreeNode s,TreeNode t){
        if (s==null || t==null){
            return s == null && t == null;

        }else if (s.val == t.val){
            return isTheSame(s.left,t.left) && isTheSame(s.right,t.right);
        }else {
            return false;
        }

    }





    //1
    public int[] twoSum(int[] num, int traget) {
        Map<Integer, Integer> num_map = new HashMap<>();
        for (int i = 0; i < num.length; i++) {
            int cmpt = traget - num[i];
            if (num_map.containsKey(cmpt)) {
                return new int[]{num_map.get(cmpt), i};
            }
            num_map.put(num[i], i);

        }

        throw new IllegalArgumentException("not found");
    }






    //15
    public ArrayList<ArrayList<Integer>>threeSum(int[]num){
        ArrayList<ArrayList<Integer>>result=new ArrayList<>();
        if (num.length<3) {
            return result;
        }
        Arrays.sort(num);
        for (int i =0;i<num.length;i++){
            if (i!=0 && num[i]==num[i-1])
                continue;
            int left =i+1;
            int right = num.length-1;
            while (left<right){
                int sum=num[i]+num[left]+num[left];

                if (sum>0){
                    left++;
                }else if (sum<0){
                    right--;
                }else {
                    ArrayList<Integer>temp =new ArrayList<>();
                    temp.add(num[i]);
                    temp.add(num[left]);
                    temp.add(num[right]);
                    result.add(temp);
                }
            }
            do {
                left++;
            }while (left < right && num[left] == num[left-1]);
            do {
                right--;
            } while (right > left && num[right] == num[right+1]);
        }
        return result;
    }


}
